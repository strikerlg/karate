<?php
/*
 * Template Name: Летняя школа
 */

get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<h1 class="entry-title col12"><?php the_title(); ?></h1>
						<div class="col8 entry-content">
							<?php the_content(); ?>
<?php endwhile; ?>
						</div><!-- .entry-content -->
						<div class="col4 last">
							<aside class="near-trips">
								<h3 class="near-trips_title">Анонс</h3>

							<?php
								$events = eo_get_events(array(
					       'numberposts'=>5,
					       // 'event_start_after'=>'today',
					       'event-category'=>'poezdki',
    						));
    						if($events){
    							global $post;
						     echo '<div class="near-trip">';
						      foreach( $events as $post ):
						      	 $event_start_month = $monthes[eo_get_the_start('n')];
						      	 $event_end_month = $monthes[eo_get_the_end('n')];
         						setup_postdata($post);?>
						             <div class="near-trip_date">
						            	c <?php echo eo_the_start('j');
						            		if (!($event_end_month==$event_start_month)) echo ' ' . $event_start_month . ' <br>';
						            			?>&nbsp;по <?php echo (eo_the_end('j') . ' ' . $event_end_month);?>
						             </div>
						             <div class="near-trip_link">
						             	<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						             </div>
													<div class="near-trip_text"><?php the_excerpt(); ?></div>
						         <?php
						  	  endforeach;
						     	echo '</div>';
						  	}else{
						  		echo 'В ближайшее время поездки не планируются';
						  	}
    					?>
							</aside>
						</div>
				</article><!-- #post-## -->

</div>
<?php get_footer(); ?>
