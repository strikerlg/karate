<?php
/*
 * Template Name: Заглушка
 */

?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html <?php language_attributes(); ?> class="no-js ie ie6 lte7 lte8 lte9"><![endif]-->
<!--[if IE 7 ]><html <?php language_attributes(); ?> class="no-js ie ie7 lte7 lte8 lte9"><![endif]-->
<!--[if IE 8 ]><html <?php language_attributes(); ?> class="no-js ie ie8 lte8 lte9"><![endif]-->
<!--[if IE 9 ]><html <?php language_attributes(); ?> class="no-js ie ie9 lte9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <!-- <meta name="viewport" content="width=device-width"> -->
    <title><?php wp_title( '|', true, 'right' );?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11" />

    <link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
       <?php wp_head(); ?>
      </head>
  <body <?php body_class(); ?>>
      <section class="container">
        <div class="row">
          <h1 class="col12 champ_header">
           <span class="champ_date">СОСТОЯЛСЯ</span>
          </h1>
        </div>
        <div class="row">
          <div class="col2">
            <a class="logo-img" href="<?php echo home_url( '/' ); ?>" title="<?php bloginfo( 'description' ); ?>" rel="home">
              <img src="<?php bloginfo( 'template_directory' ); ?>/images/champ_logo1.png" alt="<?php bloginfo( 'description' ); ?>">
            </a>
          </div>
          <div class="col8">
            <h2 class="champ_header2">Открытый чемпионат России</h2>
            <p class="champ_skkr">СОЮЗА КИОКУШИН КАРАТЭ РОССИИ</p>
          </div>
          <div class="col2 last"><img src="<?php bloginfo( 'template_directory' ); ?>/images/champ_logo.png" alt="<?php bloginfo( 'description' ); ?>"></div>
        </div>
        <div class="row">
          <div class="col2"><p class="champ_p">rus</p></div>
          <div class="col8">
            <img class="champ_fighters" src="<?php bloginfo( 'template_directory' ); ?>/images/champ_fighters.png" alt="<?php bloginfo( 'description' ); ?>">
          </div>
          <div class="col2 last"><p class="champ_p"><a class="champ_link" href="/championat-eng">eng</a></p></div>
        </div>
        <div class="row">
          <div class="col12">
            <h2 class="champ_kiokushin">КИОКУШИН КАРАТЭ</h2>
            <div class="bullets"><a class="modalLink modal_puli" href="#modal">Посмотреть пули к чемпионату</a></div>
            <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
            <div id="modal" class="modal">
              <div class="closeBtn"></div>
              <h2><?php the_title(); ?></h2>
              <div class="modal_protocols"><?php the_content(); ?></div>
            </div>
            <div class="overlay"></div>
             <?php  endwhile; ?>
            <a href="/glavnaya" class="champ_href-skkr">НА САЙТ СККР ></a>
          </div>
        </div>
      </section>
       <script src="<?php bloginfo( 'template_directory' ); ?>/js/jquery1-10-1.min.js"></script>
  <script src='<?php bloginfo( 'template_directory' ); ?>/js/jquery.modal.min.js'></script>
  <script>
  $(document).ready(function(){
    $('.modalLink').modal({
        trigger: '.modalLink',      // id or class of link or button to trigger modal
        olay:'div.overlay',         // id or class of overlay
        modals:'div.modal',         // id or class of modal
        animationEffect: 'fadeIn',  // overlay effect | slideDown or fadeIn | default=fadeIn
        animationSpeed: 200,        // speed of overlay in milliseconds | default=400
        moveModalSpeed: 'slow',     // speed of modal movement when window is resized | slow or fast | default=false
        background: '000',          // hexidecimal color code - DONT USE #
        opacity: 0.6,               // opacity of modal |  0 - 1 | default = 0.8
        openOnLoad: false,          // open modal on page load | true or false | default=false
        docClose: true,             // click document to close | true or false | default=true
        closeByEscape: true,        // close modal by escape key | true or false | default=true
        moveOnScroll: false,         // move modal when window is scrolled | true or false | default=false
        resizeWindow: true,         // move modal when window is resized | true or false | default=false
        close:'.closeBtn'           // id or class of close button
    });
  });</script>
      <?php wp_footer(); ?>
  </body>
</html>