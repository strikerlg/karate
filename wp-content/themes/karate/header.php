<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Boilerplate
 * @since Boilerplate 1.0
 */
?><!DOCTYPE html>
<!--[if lt IE 7 ]><html <?php language_attributes(); ?> class="no-js ie ie6 lte7 lte8 lte9"><![endif]-->
<!--[if IE 7 ]><html <?php language_attributes(); ?> class="no-js ie ie7 lte7 lte8 lte9"><![endif]-->
<!--[if IE 8 ]><html <?php language_attributes(); ?> class="no-js ie ie8 lte8 lte9"><![endif]-->
<!--[if IE 9 ]><html <?php language_attributes(); ?> class="no-js ie ie9 lte9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <!-- <meta name="viewport" content="width=device-width"> -->
    <title><?php wp_title( '|', true, 'right' );?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11" />

    <link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
  <?php $theid = get_the_ID() ?>
<?php if( $theid == 46 ){ ?>
    <div class="header-slider-block">
      <div id="viewport">
        <div class="header-slider-box">
        <?php
          $args = array( 'post_type' => 'slide');
          $loop = new WP_Query( $args );
          $slides_number = $loop->found_posts - 1;
            while ( $loop->have_posts() ) : $loop->the_post();
            $thumb_url = wp_get_attachment_url(get_post_thumbnail_id());?>
              <div class="slide" style="background:url(<?php  echo $thumb_url ?>) no-repeat top center">
                  <div class="container">
                    <div class="row">
                      <div class="col8"></div>
                      <div class="col4 last header-slider_content">
                        <h3 class="header-slider-title"><?php the_title(); ?></h3>
                        <p class="header-slider-text"><?php the_content(); ?></p>
                      </div>
                    </div>
                  </div>
              </div>
        <?php endwhile; ?>
        </div>
      </div>
    </div>
<?php } ; ?>
    <header class="container">
      <div class="row site-header">
        <div class="col3 col3s logo last">
          <h1>
            <a class="logo-img" href="<?php echo home_url( '/' ); ?>" title="<?php bloginfo( 'description' ); ?>" rel="home"><img src="<?php bloginfo( 'template_directory' ); ?>/images/logo.png" alt="<?php bloginfo( 'description' ); ?>"></a>
          </h1>
           <!-- Ссылка на вступление в организацию -->
          <?php if( $theid == 46 ){ ?>
           <div class="header-enlist">
						<?php
							$page_id = 482;
					    $enlist_page = get_page( $page_id );
					    $content = $enlist_page->post_content;
					    if ( ! $content ) // Check for empty page
					    continue;
					    $content = apply_filters( 'the_content', $content );
					  ?>
            <p><a class="header-enlist-a modalLink" href="#modal-enlist">Хотите вступить<br> в нашу организацию?</a></p>
            <div id="modal-enlist" class="modal">
              <div class="closeBtn"></div>
              <h2><?php echo $enlist_page->post_title; ?></h2>
              <!--  <div class="alignleft modal_thumb"><?php echo get_the_post_thumbnail($enlist_page->ID, array(188,240)); ?></div> -->
            <div class="modal_text"><?php echo $content; ?></div>
            </div>
           </div>
           <div class="overlay"></div>
          <?php }; ?>
           <!-- Ссылка на вступление в организацию -->
        </div>
        <nav class="main-nav <?php if( $theid == 46 ) echo "col5"; else echo "col5"; ?>" id="access" role="navigation">
          <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary','items_wrap' => '<ul id="nav" class="%2$s">%3$s</ul>' ) ); ?>
        </nav><!-- #access -->
          <div class="<?php if( $theid == 46 ) echo "col4"; else echo "col4"; ?> social-and-languages last">

            <ul class="social-btns">
            	<li class="soc-btn" >
	            	<div class="top-searchform">
	            		<?php get_search_form(); ?>
								</div>
							</li>
              <li class="soc-btn soc-btn_fb" ><a class="soc-btn_a" href="http://www.facebook.com/profile.php?id=100001152116145"></a></li>
              <li class="soc-btn soc-btn_vk" ><a class="soc-btn_a" href="http://vk.com/club47541840"></a></li>
              <li class="soc-btn soc-btn_odk"><a class="soc-btn_a" href="http://www.odnoklassniki.ru/profile/361650133159"></a></li>

            </ul>
            <br class="clear">
            <?php if( $theid == 46 ){ ?>
              <ul  id="controls" class="header-slider-buttons">
                <?php  $i=0;
                  while ($i <= $slides_number) { ?>
                    <li class='header-slider-button'>
                      <a class="goto-slide <?php if ($i == 0) echo 'current' ?>" href="#" data-slideindex="<?php echo $i ?>"></a>
                    </li>
                  <?php $i++;  }}
                ?>
              </ul>
        </div>
      </div>
    </header>
    <section id="content" role="main">
      <div class="container <?php if( $theid == 46 ) echo "newsblock-bg"; ?>">
        <div class="row">